
Photo Reader
============


Initial Raspberry Pi
---------------------
* Install Python3, Python3-pip, Python3-Virtaulenv

> apt-get install python3 virtualenv python3-pip

Install pip modules
--------------------

> pip3 install inotify
> pip3 install dirsync


How to deploy in Raspberry Pi
------------------------------

(1) With Virtualenv

Create Virtualenv

> virutalenv -p python3 cpg-env

Enable & Into Virtualenv

> $ source cpg-env/bin/activate




import os
import inotify.adapters
import datetime
from pprint import pprint
from dirsync import sync as dsync
import glob
import sys

sys.path.append("../")
sys.path.append("./")

import modules.config.core as cfg



'''
VER 0.1
Author HaWay

Flow:
(1) Read config
(2) Check value in config
(3) start inotify
(4) waiting CF card
'''

def process_sync( source_path, target_name, jcfg ):
    print("process_sync")
    print("source_path :" + source_path + " target_name :" + target_name )
    print("pname:" + jcfg["system"]["pname"])

    target_data = jcfg["settings"]["targets"][target_name]
    new_path  = str( target_data["dirtag"] ).replace( '{YEAR}', str( datetime.datetime.now().year ) )
    new_path = new_path.replace('{MONTH}',  str( datetime.datetime.now().month ))
    new_path = new_path.replace('{DAY}', str(datetime.datetime.now().day ))
    new_path = new_path.replace('{HH}', str( datetime.datetime.now().hour ))
    new_path = new_path.replace('{CNAME}', jcfg["conf"]["name"])
    new_path = new_path.replace('{PNAME}', jcfg["system"]["pname"])

    print ( "Source_Path:" + source_path + ", target_path:" + new_path )
    dsync( source_path, new_path, 'sync', create=True)

    return True

def process_flickr( sourcedir, jcfg ):
    print("process_flickr")
    return True


def get_pname( source_path ):
    pnames = glob.glob( source_path + "/**/*.cpg", recursive=True)
    return str( os.path.basename( pnames[0] ) ).rstrip('pgc.')


def processtarget( act_name, pjcfg ):

    print( "act_name:" + act_name )

    a_sor_cfg = pjcfg['settings']['watchdirlist'][act_name]
    targets_group = pjcfg['settings']['targets']

    # Get photographer name
    pname = get_pname( a_sor_cfg["watchdir"] )
    pjcfg["system"]["pname"] = pname

    print( "Photographer name is : " + pjcfg["system"]["pname"] )

    for sor_target in pjcfg['settings']['watchdirlist'][act_name]["targets"]:
        print("sor_target:" + sor_target)
        print('target_group:[sor_target][copy] :' + pjcfg["settings"]["targets"][sor_target]["copy"])

        if ( pjcfg["settings"]["targets"][sor_target]["copy"] == "sync" ):
            if ( os.path.exists(targets_group[sor_target]["dir"]) ):
                process_sync( a_sor_cfg["watchdir"], sor_target, pjcfg )
            else:
                print("Error dir not exist:" + targets_group[sor_target]["dir"])

            continue

        if ( targets_group[act_name]["copy"] == "flickr" and targets_group[sor_target]["account"] ):
            process_flickr( a_sor_cfg["watchdir"], pjcfg )
            continue

        print("Error No target")


def _startwatchdir():

    print("Start inotify")
    i = inotify.adapters.Inotify()

    watchdirs = jcfg["settings"]['watchdirlist']

    for name in watchdirs:
        wdir = watchdirs[name]["watchdir"]
        if not os.path.isdir( wdir ):
            print( "Error Directory not exist:" + str(wdir) )
            continue

        i.add_watch(bytes( wdir, encoding='UTF-8') )

    #i.add_watch( bytes( jcfg["settings"]["watchdir"], encoding='UTF-8' ) )

    try:
        for event in i.event_gen():

            if event is not None:
                (header, type_names, m_path, filename) = event
                print ("I got a event")
                print (event)
                print ("Path: %s" % m_path)
                print ("filename %s" % filename)
                print ("typename: %s" % type_names )

                if "IN_CREATE" in type_names:
                    print("Create DIR!!")

                k = m_path.decode('UTF-8')

                for name in watchdirs:
                    print( watchdirs[name]["watchdir"]  + ":" + k )
                    if ( watchdirs[name]["watchdir"] == k  ):
                        processtarget( name, jcfg )


                '''
                print ("WD=(%d) MASK=(%d) COOKIE=(%d) LEN=(%d) MASK->NAMES=%s WATCH-PATH=[%s] FILENAME=[%s]", + \
                       header.wd, header.mask, header.cookie, header.len, type_names, + \
                             watch_path, filename)
                '''


    finally:
        print ("Finally")
        return



config = cfg.objConfig()
config.ReadConfig(r'../config/config.json')
jcfg = config.ConvertJson()

if __name__=='__main__':
    _startwatchdir()

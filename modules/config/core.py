import json

class objConfig:

    config_path = r'./config/config.json'

    def __init__(self):
        self.config_text = ''
        self.config_json = ''


    def ReadConfig(self, newconfpath=None):

        config_text2 = ''

        if ( newconfpath is None ):
            rfile = self.config_path
        else:
            rfile = newconfpath

        try:
            for line in open( rfile, 'r', encoding='UTF-8'):
                config_text2 += line
        except:
            print("Error of open config file:" + rfile)
            return False

        self.config_text = config_text2      # replace data when read success.
        return self.config_text


    def ConvertJson(self, org_text=None):
        if ( org_text is None ):
            cdata = self.config_text
        else:
            cdata = org_text

        try:
            self.config_json = json.loads( cdata )
        except:
            self.config_json = False
            print("ConvertJson Fail")
            return False

        return self.config_json


    def __str__(self):
        return json.dumps(self.config_json)
